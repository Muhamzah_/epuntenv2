import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
import InfiniteLoading from 'vue-infinite-loading';
import '@babel/polyfill';
import "core-js/stable";
import "regenerator-runtime/runtime";
import VueHighlightJS from 'vue-highlightjs';
import { localize } from 'vee-validate';
import id from 'vee-validate/dist/locale/id.json';
import * as VeeValidate  from 'vee-validate';
import { ValidationProvider } from 'vee-validate';
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";

Vue.component("v-select", vSelect);


localize({
  id
});

Vue.use(VueHighlightJS)

Vue.use(VeeValidate);

Vue.use(InfiniteLoading, {
  slots: {
    noMore: 'Tidak ada lagi data', // you can pass a string value
    error: 'Gagal mengambil data', // you also can pass a Vue component as a slot
    noResults: '',
  },
});

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)

Vue.use(VueAxios, axios)

window.axios = require("axios");
window.axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
window.axios.defaults.headers.common["Authorization"] =
"Bearer " + localStorage.getItem("tokenEpunten");
/*let apiToken = "Bearer " + localStorage.token;*/
// window.axios.defaults.baseURL = "https://disdukcapil.bandung.go.id/";
// window.axios.defaults.baseURL = "//localhost/e-punten";
window.axios.defaults.baseURL = "//10.32.73.7:8080/Api/api";

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
  console.error(
    "CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token"
  );
}
router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.auth) && !store.state.isLoggedIn) {
    next({ path: "/pages/Login" });
    return;
  }

  if (to.path === "/pages/Login" && store.state.isLoggedIn) {
    next({ path: "/" });
    return;
  }

  if (to.path === "/reset-pass" && store.state.isLoggedIn) {
    next({ path: "/" });
    return;
  }

  if (to.path === "/pages/Register" && store.state.isLoggedIn) {
    next({ path: "/" });
    return;
  }

  next();
});

new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  vuetify,
   InfiniteLoading,


  components: {
    App,
    ValidationProvider,
    vSelect,
  }

})
