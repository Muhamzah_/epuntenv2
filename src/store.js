import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// const state = {
//   sidebarShow: 'responsive',
//   sidebarMinimize: false
// }
//
// const mutations = {
//   toggleSidebarDesktop (state) {
//     const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
//     state.sidebarShow = sidebarOpened ? false : 'responsive'
//   },
//   toggleSidebarMobile (state) {
//     const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
//     state.sidebarShow = sidebarClosed ? true : 'responsive'
//   },
//   set (state, [variable, value]) {
//     state[variable] = value
//   }
// }

export default new Vuex.Store({
  // state,
  // mutations,
  state: {
  sidebarShow: 'responsive',
  sidebarMinimize: false,
  drawer: false,
  isLoggedIn: !!localStorage.getItem("tokenEpunten"),
  loggedInItems: [
    { text: 'Beranda', to: '/', icon:'fas fa-home'},
    //{ text: 'Daftar', to: '/daftar', icon: 'fas fa-file-signature'},
    //{ text: 'Login', to: '/login', icon: 'fas fa-lock'},
    { text: 'Profil', to: '/profil', icon: 'fas fa-user-circle'}

  ],
  notLoggedInItems: [
    { text: 'Beranda', to: '/', icon:'fas fa-home'},
    { text: 'Daftar', to: '/daftar', icon: 'fas fa-file-signature'},
  //  { text: 'Login', to: '/login', icon: 'fas fa-lock'},
    //{ text: 'Profil', to: '/profil', icon: 'fas fa-user-circle'}

  ]
},
getters:{
  links: (state) => {
    if (state.isLoggedIn) {
      return state.loggedInItems
      //this.loggedIn = true
      //this.getKontak();
    } else {
      return state.notLoggedInItems
    }

  }
},
mutations: {
  setDrawer: (state, payload) => (state.drawer = payload),
  toggleDrawer: state => (state.drawer = !state.drawer),
  loginUser(state) {
    state.isLoggedIn = true;
  },
  logoutUser(state) {
    state.isLoggedIn = false;
  },
  toggleSidebarDesktop (state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  toggleSidebarMobile (state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set (state, [variable, value]) {
    state[variable] = value
  }
},
actions: {

}
})
